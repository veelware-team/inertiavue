<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;

class LeadController extends Controller
{
    public function index()
    {
        $leads = Lead::query()
                 ->where('branch_id',1)
                 ->orderByDesc('id')
                 ->get();

        return Inertia::render('Private/Leads/Index', ['leads'=>$leads]);
    }

    public function create()
    {
        return Inertia::render('Private/Leads/LeadAdd');
    }

    public function store(Request $request)
    {
        $postData = $this->validate($request, [
           'name' => 'required',
           'email' => 'required|email',
           'dob' => 'required|date',
           'phone' => 'required',
        ]);

        $package = "";
        if($request->has('package')){
            $package = $request->input('package');
        }
        $dob = Carbon::parse($postData['dob']);
        $age = $dob->age;
        Lead::create([
            'name' => $postData['name'],
            'email' => $postData['email'],
            'dob' => $dob,
            'phone' => $postData['phone'],
            'branch_id' => 1,
            'age' => $age,
            /*'added_by' => Auth::user()->id,*/
            'added_by' => 1,
            'interested_package' => $package,
        ]);

        return redirect()->route('lead.index');
    }

    public function show(Lead $lead)
    {
        $lead->load(['reminders']);
        return Inertia::render('Private/Leads/Show',['lead-prop' => $lead]);
    }

    public function update(Request $request, Lead $lead){
        $postData = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'dob' => 'required|date',
            'phone' => 'required',
        ]);
        $postData = Carbon::parse($postData['dob'])->age;
        $lead->update($postData);
        return redirect()->route('lead.index');
    }

    public function destroy(Lead $lead){
        $lead->delete();
        return redirect()->route('lead.index');
    }

/*    public function show(Curso $curso){
        return view('cursos.show', compact('curso'));
    }

    public function edit(Curso $curso){
        return view('cursos.edit', compact('curso'));
    }

    public function update(Request $request, Curso $curso){
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
            'categoria' => 'required',
        ]);

        $curso->update($request->all());
        return redirect()->route('cursos.show', $curso);
    }

    public function destroy(Curso $curso){
        $curso->delete();
        return redirect()->route('cursos.index');
    }*/
}
