<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Reminder;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ReminderController extends Controller
{
    public function add(Lead $lead)
    {
        return Inertia::render('Private/Leads/LeadReminderAdd', ['lead'=>$lead] );
    }

    public function store(Request $request)
    {
        $postData = $this->validate($request,[
            'reminder'=>'required|min:3',
            'reminder_date'=>'required|date',
            'lead_id'=>'required|exists:leads,id',
        ]);
        $postData['user_id'] = 1;
        $postData['status'] = 'Pending';
        $lead = Lead::find($postData['lead_id']);

        $lead->reminders()->create($postData);

        return redirect()->route('lead.show',[$lead]);
    }

    public function show(Lead $lead,Reminder $reminder)
    {
        return Inertia::render('Private/Leads/ReminderShow',['lead'=>$lead,'reminder'=>$reminder]);
    }

    public function update(Request $request)
    {
        $postData = $this->validate($request,[
           'id'=>'required| |exists:reminders,id'
        ]);
        $reminder = Reminder::find($postData['id']);
        $reminder->status = 'completed';
        $reminder->save();

        $lead = Lead::find($reminder->lead_id);
        return redirect()->route('reminder.add',['lead'=> $lead]);
    }

    public function addNote(Lead $lead, Reminder $reminder)
    {
        return Inertia::render('Private/Leads/ReminderNote',[
           'lead'=>$lead,
           'reminder'=>$reminder
        ]);
    }

    public function close(Request $request)
    {
        $postData = $this->validate($request,[
            'reminder_id'=>'required| |exists:reminders,id',
            'note'=>'required|min:3',
        ]);

        $reminder = Reminder::find($postData['reminder_id']);
        $reminder->status= 'completed';
        $reminder->save();

        $lead = Lead::find($reminder->lead_id);
        return redirect()->route('lead.show',[$lead]);
    }
}
