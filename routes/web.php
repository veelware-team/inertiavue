<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\DashboardController;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/dashboard',[DashboardController::class,'index'])->name('dash');
Route::get('/create',[DashboardController::class,'create'])->name('create');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*
Route::group(['middleware' =>'auth'], function () {

});*/

Route::get('/leads',[\App\Http\Controllers\LeadController::class,'index'])->name('lead.index');
Route::get('/leads/show/{lead}',[\App\Http\Controllers\LeadController::class,'show'])->name('lead.show');
Route::get('/leads/create',[\App\Http\Controllers\LeadController::class,'create'])->name('lead.create');
Route::post('/leads/save',[\App\Http\Controllers\LeadController::class,'store'])->name('lead.store');
Route::put('/leads/{lead}',[\App\Http\Controllers\LeadController::class,'update'])->name('lead.update');
Route::delete('/leads/{lead}',[\App\Http\Controllers\LeadController::class,'destroy'])->name('lead.destroy');

Route::get('/leads/show/{lead}/reminder/add',[\App\Http\Controllers\ReminderController::class,'add'])->name('reminder.add');
Route::post('/leads/show/reminder/save',[\App\Http\Controllers\ReminderController::class,'store'])->name('reminder.save');
Route::get('/leads/show/{lead}/reminder/{reminder}/show',[\App\Http\Controllers\ReminderController::class,'show'])->name('reminder.show');
Route::post('/leads/show/reminder/update-create',[\App\Http\Controllers\ReminderController::class,'update'])->name('reminder.update');
Route::get('/leads/view/{lead}/reminder/{reminder}/note',[\App\Http\Controllers\ReminderController::class,'addNote'])->name('reminder.note');
Route::post('/leads/view/reminder/close',[\App\Http\Controllers\ReminderController::class,'close'])->name('reminder.close');

