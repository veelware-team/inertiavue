@component('mail::message')
# Recordatorios por atender.

Tu tienes algunos recordatorios para seguir, aca estan los detalles:

@component('mail::table')
|Reminder| Lead Name| Phone|
|:-------|:-------|:-------|
@foreach($reminders as $reminder)
|{{$reminder['reminder']}}|{{$reminder['lead']['name']}}|{{$reminder['lead']['phone']}}|
@endforeach
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
