const Ziggy = {"url":"http:\/\/inertiavue.test","port":null,"defaults":{},"routes":{"login":{"uri":"login","methods":["GET","HEAD"]},"logout":{"uri":"logout","methods":["POST"]},"register":{"uri":"register","methods":["GET","HEAD"]},"password.request":{"uri":"password\/reset","methods":["GET","HEAD"]},"password.email":{"uri":"password\/email","methods":["POST"]},"password.reset":{"uri":"password\/reset\/{token}","methods":["GET","HEAD"]},"password.update":{"uri":"password\/reset","methods":["POST"]},"password.confirm":{"uri":"password\/confirm","methods":["GET","HEAD"]},"verification.notice":{"uri":"email\/verify","methods":["GET","HEAD"]},"verification.verify":{"uri":"email\/verify\/{id}\/{hash}","methods":["GET","HEAD"]},"verification.resend":{"uri":"email\/resend","methods":["POST"]},"dash":{"uri":"dashboard","methods":["GET","HEAD"]},"create":{"uri":"create","methods":["GET","HEAD"]},"home":{"uri":"home","methods":["GET","HEAD"]},"lead.add":{"uri":"leads\/add","methods":["GET","HEAD"]},"lead.store":{"uri":"leads\/save","methods":["POST"]},"lead.index":{"uri":"leads","methods":["GET","HEAD"]},"lead.show":{"uri":"leads\/show\/{lead}","methods":["GET","HEAD"],"bindings":{"lead":"id"}}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    for (let name in window.Ziggy.routes) {
        Ziggy.routes[name] = window.Ziggy.routes[name];
    }
}

export { Ziggy };
