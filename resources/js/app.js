require('./bootstrap');
import Vue from 'vue'
import { App, plugin } from '@inertiajs/inertia-vue'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import 'boxicons'

// Vue.config.productionTip = false
Vue.mixin({ methods: { route: window.route } })
Vue.use(plugin)
Vue.use(Vuesax, {
    colors: {
        primary:'#49C4D1',
        success:'rgb(23, 201, 100)',
        danger:'rgb(242, 19, 93)',
        warning:'rgb(255, 130, 0)',
        dark:'rgb(36, 33, 69)'
    }
})
const el = document.getElementById('app')
if(el){
    new Vue({
        render: h => h(App, {
            props: {
                initialPage: JSON.parse(el.dataset.page),
                resolveComponent: name => require(`./Pages/${name}`).default,
            },
        }),
    }).$mount(el)
}
