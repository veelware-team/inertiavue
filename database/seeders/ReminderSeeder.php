<?php

namespace Database\Seeders;

use App\Models\Reminder;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ReminderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reminder::create([
            'lead_id' => 1,
            'user_id' => 1,
            'reminder' => 'Un recordatorio 1',
            'reminder_date' => Carbon::now()->subDays(3),
            'note' => 'Una nota 1',
            'status'=>'completed',


        ]);

        Reminder::create([
            'lead_id' => 2,
            'user_id' => 2,
            'reminder' => 'Un recordatorio 2',
            'reminder_date' => Carbon::now()->subDays(4),
            'note' => 'Una nota 2',
            'status'=>'pending',
        ]);

        Reminder::create([
            'lead_id' => 1,
            'user_id' => 1,
            'reminder' => 'Un recordatorio 1',
            'reminder_date' => Carbon::now()->subDays(2),
            'note' => 'Una nota 1',
            'status'=>'completed',
        ]);
    }
}
