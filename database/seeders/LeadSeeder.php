<?php

namespace Database\Seeders;

use App\Models\Lead;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LeadSeeder extends Seeder
{
    public function run()
    {
        Lead::create([
           'name'=>'James Smith',
           'email'=>'james@example.com',
           'phone'=>'44552200',
           'interested_package'=>'Annual plan',
           'dob'=> Carbon::parse('02/01/1986'),
           'age'=> Carbon::parse('02/01/1986')->age,
           'branch_id'=> 1,
           'added_by'=> 1,
        ]);

        Lead::create([
            'name'=>'Jhon Done',
            'email'=>'jhon@example.com',
            'phone'=>'46552200',
            'interested_package'=>'Monthly plan',
            'dob'=> Carbon::parse('02/01/1982'),
            'age'=> Carbon::parse('02/01/1982')->age,
            'branch_id'=> 1,
            'added_by'=> 1,
        ]);

        Lead::create([
            'name'=>'Maria Parker',
            'email'=>'maria@example.com',
            'phone'=>'45565620',
            'interested_package'=>'Monthly plan',
            'dob'=> Carbon::parse('02/01/1984'),
            'age'=> Carbon::parse('02/01/1984')->age,
            'branch_id'=> 1,
            'added_by'=> 1,
        ]);

        Lead::create([
            'name'=>'Carlos Morales',
            'email'=>'cm@example.com',
            'phone'=>'45660022',
            'interested_package'=>'Annual plan',
            'dob'=> Carbon::parse('02/01/1993'),
            'age'=> Carbon::parse('02/01/1993')->age,
            'branch_id'=> 1,
            'added_by'=> 1,
        ]);

        Lead::create([
            'name'=>'Sergio Estrada',
            'email'=>'sestreada@example.com',
            'phone'=>'45788899',
            'interested_package'=>'Annual plan',
            'dob'=> Carbon::parse('02/01/1994'),
            'age'=> Carbon::parse('02/01/1994')->age,
            'branch_id'=> 1,
            'added_by'=> 1,
        ]);
    }
}
